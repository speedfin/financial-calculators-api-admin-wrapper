<?php

namespace Speedfin\Calculators\Admin\Common\Service;

interface JWTInterface
{
    public function setToken(string $token): void;
}
