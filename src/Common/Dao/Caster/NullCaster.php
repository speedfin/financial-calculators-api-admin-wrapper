<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class NullCaster implements Caster
{
    public function __construct(private array $type)
    {
    }

    public function cast(mixed $value): mixed
    {
        return $value === "" || $value === null ? null : $value;
    }
}
