<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;
use DateTime;

class DateTimeCaster implements Caster
{
    public function __construct(private array $type, private string $format)
    {
    }

    public function cast(mixed $value): ?DateTime
    {
        if (!is_string($value)) return null;

        $value = DateTime::createFromFormat($this->format, $value);
        if (!$value) $value = null;

        return $value;
    }
}
