<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class JsonDecodeCaster implements Caster
{
    public function __construct(
        private array $type
    ) {
    }

    public function cast(mixed $value): ?array
    {
        return is_array($value) ? $value : json_decode((string) $value, true);
    }
}
