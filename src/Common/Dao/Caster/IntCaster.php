<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class IntCaster implements Caster
{
    public function __construct(private array $type)
    {
    }

    public function cast(mixed $value): ?int
    {
        return $value === "" || $value === null ? null : (int) $value;
    }
}
