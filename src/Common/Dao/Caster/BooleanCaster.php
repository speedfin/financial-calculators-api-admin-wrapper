<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class BooleanCaster implements Caster
{
    public function __construct(private array $type)
    {
    }

    public function cast(mixed $value): ?bool
    {
        return $value === null ? null : (bool) $value;
    }
}
