<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\NullCaster;

abstract class AbstractProductMarginDto extends AbstractItemDto
{
    #[CastWith(IntCaster::class)]
    public ?int $valueFrom;
    #[CastWith(IntCaster::class)]
    public ?int $valueTo;
    #[CastWith(NullCaster::class)]
    public ?string $margin;
    #[CastWith(NullCaster::class)]
    public ?string $provision;
    #[CastWith(NullCaster::class)]
    public ?string $marginMin;
}