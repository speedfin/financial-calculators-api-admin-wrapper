<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\DateTimeCaster;
use \DateTime;
use Speedfin\Calculators\Admin\Common\Dao\Caster\JsonDecodeCaster;

abstract class AbstractProductCorrectionDto extends AbstractItemDto
{
    public ?string $name;
    public ?string $displayName;
    public ?string $description;
    public ?string $type;
    public ?string $paymentValueType;
    public ?string $marginRangeData;
    public ?string $paymentRangeData;
    public ?int $logicSequencePriority;
    public ?int $period;
    public ?int $periodFrom;
    public ?int $correctionPeriod;
    public ?int $correctionPeriodFrom;
    public ?int $frequency;
    public ?int $paymentPeriod;
    public ?int $paymentPeriodFrom;
    public ?int $ageFrom;
    public ?int $ageTo;
    public ?int $orderNumber;
    public ?string $firm;
    public ?string $provision;
    public ?string $provisionForced;
    public ?string $margin;
    public ?string $marginForced;
    public ?string $paymentValue;
    public ?bool $active;
    public ?bool $required;
    public ?bool $checkNotRequired;
    public ?bool $onlyInfo;
    public ?bool $logicSequenceRequired;
    public ?bool $creditingPossibility;
    public ?bool $enabledAgeType;
    public ?bool $enabledFilterType;
    public ?bool $displayFilterType;
    #[CastWith(JsonDecodeCaster::class)]
    public ?array $filtersData;

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setRequired(?bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function setOnlyInfo(?bool $onlyInfo): self
    {
        $this->onlyInfo = $onlyInfo;

        return $this;
    }
    
    public function getFiltersData(): ?array
    {
        return $this->filtersData;
    }

    public function setFiltersData(?array $filtersData): self
    {
        $this->filtersData = $filtersData;

        return $this;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}