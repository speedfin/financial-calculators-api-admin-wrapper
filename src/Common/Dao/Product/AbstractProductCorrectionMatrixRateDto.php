<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\NullCaster;

abstract class AbstractProductCorrectionMatrixRateDto extends AbstractItemDto
{
    #[CastWith(IntCaster::class)]
    public ?int $secureValueFrom;
    #[CastWith(IntCaster::class)]
    public ?int $secureValueTo;
    #[CastWith(IntCaster::class)]
    public ?int $mortgageValueFrom;
    #[CastWith(IntCaster::class)]
    public ?int $mortgageValueTo;
    #[CastWith(IntCaster::class)]
    public ?int $periodFrom;
    #[CastWith(IntCaster::class)]
    public ?int $periodTo;
    #[CastWith(IntCaster::class)]
    public ?int $ageFrom;
    #[CastWith(IntCaster::class)]
    public ?int $ageTo;
    #[CastWith(NullCaster::class)]
    public ?string $rate;
}