<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\DateTimeCaster;
use \DateTime;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyDto;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexDto;

abstract class AbstractProductDto extends AbstractItemDto
{
    //from calculator product_type table
    const PRODUCT_TYPES = [
        'Kredyt hipoteczny' => 1,
        'Pożyczka hipoteczna' => 2
    ];
    public ?string $name;
    public ?string $comment;
    public ?string $promotionName;
    public ?string $prepaymentDescription;
    public ?string $conversionDescription;
    public ?string $additionalRequirements;
    public ?string $accountManagement;
    public ?string $prosCons;
    public ?bool $visible;
    public ?bool $archived = false;
    public ?bool $promotion;
    public bool $prepaymentAvailable = true;
    public bool $conversionAvailable = false;
    public bool $provisionCredited = false;
    public ?bool $propertyPreliminaryDecision;
    public ?bool $propertyNegotiateAbility;
    public ?bool $propertyVacationCredit;
    public ?bool $propertyBailiffPurpose;
    public ?bool $propertySeasonalIncome;
    public ?bool $propertyRequiredExcerpt;
    public ?bool $propertyEarlyRepayment;
    public ?bool $propertyMaternal;
    public ?bool $propertyThirdPersonSecurity;
    public ?bool $propertyAverageAgeWeighted;
    public ?bool $propertyPossibleDelays;
    public ?bool $propertyPossibleActivityLoss;
    public ?bool $propertyNotRequiredTransfers;
    public ?bool $propertyDriverIncome;
    public ?bool $propertyAcceptedValuation;
    public ?bool $propertyProlongation;
    public ?bool $propertyAdditionalAmount;
    public ?bool $propertyCreditCardRequired;
    public ?int $creditingPeriodMin;
    public ?int $creditingPeriodMax;
    public ?int $gracePeriod;
    public ?int $clientAgeMin;
    public ?int $clientAgeMax;
    public ?int $toAge;
    public ?int $ltvMax;
    public ?string $bank;
    public ?BankDto $bankItem;
    public ?string $currency;
    public ?CurrencyDto $currencyItem;
    public ?string $currencyIndex;
    public ?CurrencyIndexDto $currencyIndexItem;
    #[CastWith(DateTimeCaster::class, format: 'Y-m-d')]
    public $promotionDateFrom;
    #[CastWith(DateTimeCaster::class, format: 'Y-m-d')]
    public $promotionDateTo;
    public ?string $bankClient;

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function archive()
    {
        if ($this->archived === false) {
            $this->archived = true;
            $this->visible = false;
        } else {
            $this->archived = false;
        }
    }
}