<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\DateTimeCaster;
use \DateTime;
use Speedfin\Calculators\Admin\Common\Dao\Caster\JsonDecodeCaster;

abstract class AbstractProductCorrectionCombinedDto extends AbstractItemDto
{
    #[CastWith(JsonDecodeCaster::class)]
    public ?array $correctionIds;
    public ?string $provision;
    public ?string $margin;
    //for internal use of admin
    public ?int $productId;
}