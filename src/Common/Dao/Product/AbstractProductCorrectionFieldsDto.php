<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao\Product;

use Spatie\DataTransferObject\DataTransferObject;

class AbstractProductCorrectionFieldsDto extends DataTransferObject
{
    public array $types;
    public array $paymentTypes;
    public array $paymentBases;
    public array $filterPaymentBases;
}