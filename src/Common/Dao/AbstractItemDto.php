<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Admin\Common\Dao\Caster\DateTimeCaster;
use DateTime;

abstract class AbstractItemDto extends DataTransferObject
{
    public ?int $id = null;
    #[CastWith(DateTimeCaster::class, format: 'Y-m-d H:i:s')]
    public ?DateTime $created;
    #[CastWith(DateTimeCaster::class, format: 'Y-m-d H:i:s')]
    public ?DateTime $updated;
    public ?int $createdBy;
    public ?int $updatedBy;
}
