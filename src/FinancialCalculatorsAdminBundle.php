<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin;

use Speedfin\Calculators\Admin\DependencyInjection\FinancialCalculatorsExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FinancialCalculatorsAdminBundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new FinancialCalculatorsExtension();
        }

        return $this->extension;
    }
}
