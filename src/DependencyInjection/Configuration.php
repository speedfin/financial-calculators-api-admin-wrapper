<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('speedfin_calculators_admin');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('api_domain')
                    ->isRequired()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
