<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

final class FinancialCalculatorsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(\dirname(__DIR__).'/Resources/config'));
        $loader->load('services.yaml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $services = [
            'speedfin.calculators.admin.product.adapter',
            'speedfin.calculators.admin.company_loan.adapter',
            'speedfin.calculators.admin.company_loan_margin.adapter',
            'speedfin.calculators.admin.company_loan_correction.adapter',
            'speedfin.calculators.admin.company_loan_correction_matrix_rate.adapter',
            'speedfin.calculators.admin.ability.adapter',
            'speedfin.calculators.admin.ability_condition.adapter',
            'speedfin.calculators.admin.ability_module.adapter',
            'speedfin.calculators.admin.firm.adapter',
            'speedfin.calculators.admin.bank.adapter',
            'speedfin.calculators.admin.bank_employment.adapter',
            'speedfin.calculators.admin.currency.adapter',
            'speedfin.calculators.admin.commitment.adapter',
            'speedfin.calculators.admin.employment.adapter',
            'speedfin.calculators.admin.currency_index.adapter',
            'speedfin.calculators.admin.currency_index_value.adapter',
            'speedfin.calculators.admin.company_loan_filter_economic_activity.adapter',
            'speedfin.calculators.admin.company_loan_filter_security_type.adapter',
            'speedfin.calculators.admin.company_loan_filter_taxation_form.adapter',
            'speedfin.calculators.admin.company_loan_fixed_interest_rate_margin.adapter',
            'speedfin.calculators.admin.mortgage.adapter',
            'speedfin.calculators.admin.mortgage_correction.adapter',
            'speedfin.calculators.admin.mortgage_correction_combined.adapter',
            'speedfin.calculators.admin.mortgage_correction_matrix_rate.adapter',
            'speedfin.calculators.admin.mortgage_margin.adapter',
            'speedfin.calculators.admin.mortgage_fixed_interest_rate_margin.adapter',
            'speedfin.calculators.admin.mortgage_property_sort.adapter',
            'speedfin.calculators.admin.mortgage_credit_purpose.adapter',
            'speedfin.calculators.admin.money_loan.adapter',
            'speedfin.calculators.admin.money_loan_filter.adapter',
            'speedfin.calculators.admin.money_loan_correction.adapter',
            'speedfin.calculators.admin.money_loan_margin.adapter',
            'speedfin.calculators.admin.money_loan_correction_matrix_rate.adapter',
        ];

        foreach ($services as $serviceName) {
            $this->injectConfig($serviceName, $config, $container);
        }
    }

    private function injectConfig(string $serviceName, array $config, ContainerBuilder $container): void
    {
        $helperDefinition = $container->getDefinition($serviceName);
        $helperDefinition->replaceArgument(0, $config['api_domain']);
    }
    
    public function getAlias(): string
    {
        return 'speedfin_financial_calculators_admin';
    }
}
