<?php

namespace Speedfin\Calculators\Admin\CurrencyIndex\Service\CurrencyIndexValue;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueDto;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueFilter;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueListDto;

interface CurrencyIndexValueCrudServiceInterface extends JWTInterface
{
    public function update(CurrencyIndexValueDto $itemDto): CurrencyIndexValueDto;
    public function create(CurrencyIndexValueDto $itemDto): CurrencyIndexValueDto;
    public function getOne(int $id): CurrencyIndexValueDto;
    public function getAll(CurrencyIndexValueFilter $filter): CurrencyIndexValueListDto;
}
