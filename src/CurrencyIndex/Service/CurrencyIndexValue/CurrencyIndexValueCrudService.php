<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Service\CurrencyIndexValue;

use Speedfin\Calculators\Admin\CurrencyIndex\Adapter\CurrencyIndexValue\CurrencyIndexValueAdapter;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueDto;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue\CurrencyIndexValueListDto;

class CurrencyIndexValueCrudService extends AbstractCrudService implements CurrencyIndexValueCrudServiceInterface
{
    public function __construct(CurrencyIndexValueAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(CurrencyIndexValueDto $itemDto): CurrencyIndexValueDto
    {
        return new CurrencyIndexValueDto($this->createItem($itemDto));
    }

    public function update(CurrencyIndexValueDto $itemDto): CurrencyIndexValueDto
    {
        return new CurrencyIndexValueDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CurrencyIndexValueDto
    {
        return new CurrencyIndexValueDto($this->getOneItem($id));
    }
    
    public function getAll(CurrencyIndexValueFilter $filter): CurrencyIndexValueListDto
    {
        return new CurrencyIndexValueListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}
