<?php

namespace Speedfin\Calculators\Admin\CurrencyIndex\Service\CurrencyIndex;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexDto;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexFilter;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexListDto;

interface CurrencyIndexCrudServiceInterface extends JWTInterface
{
    public function update(CurrencyIndexDto $itemDto): CurrencyIndexDto;
    public function create(CurrencyIndexDto $itemDto): CurrencyIndexDto;
    public function getOne(int $id): CurrencyIndexDto;
    public function getAll(CurrencyIndexFilter $filter): CurrencyIndexListDto;
}
