<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Service\CurrencyIndex;

use Speedfin\Calculators\Admin\CurrencyIndex\Adapter\CurrencyIndex\CurrencyIndexAdapter;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexDto;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex\CurrencyIndexListDto;

class CurrencyIndexCrudService extends AbstractCrudService implements CurrencyIndexCrudServiceInterface
{
    public function __construct(CurrencyIndexAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(CurrencyIndexDto $itemDto): CurrencyIndexDto
    {
        return new CurrencyIndexDto($this->createItem($itemDto));
    }

    public function update(CurrencyIndexDto $itemDto): CurrencyIndexDto
    {
        return new CurrencyIndexDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CurrencyIndexDto
    {
        return new CurrencyIndexDto($this->getOneItem($id));
    }
    
    public function getAll(CurrencyIndexFilter $filter): CurrencyIndexListDto
    {
        return new CurrencyIndexListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}
