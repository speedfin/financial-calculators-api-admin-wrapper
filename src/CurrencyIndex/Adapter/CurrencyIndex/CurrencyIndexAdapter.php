<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Adapter\CurrencyIndex;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CurrencyIndexAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/currency_indices/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/currency_indices'),
            sprintf('%s%s', $apiDomain, '/admin/currency_indices/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_indices/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_indices/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_indices')
        );
    }
}
