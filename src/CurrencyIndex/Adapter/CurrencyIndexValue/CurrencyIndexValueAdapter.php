<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Adapter\CurrencyIndexValue;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CurrencyIndexValueAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/currency_index_values/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/currency_index_values'),
            sprintf('%s%s', $apiDomain, '/admin/currency_index_values/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_index_values/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_index_values/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currency_index_values')
        );
    }
}
