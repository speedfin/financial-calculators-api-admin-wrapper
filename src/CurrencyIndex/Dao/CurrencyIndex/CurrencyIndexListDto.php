<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CurrencyIndexListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CurrencyIndexDto::class)]
    public ?array $items = [];
}
