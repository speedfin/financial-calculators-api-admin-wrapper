<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndex;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CurrencyIndexDto extends AbstractItemDto
{
    public ?string $name;
    public ?bool $visible;
    public ?string $value;
    public ?string $currency;
}
