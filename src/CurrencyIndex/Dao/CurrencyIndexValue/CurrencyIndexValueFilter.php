<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CurrencyIndexValueFilter extends AbstractFilter
{
    public ?int $bank;
}
