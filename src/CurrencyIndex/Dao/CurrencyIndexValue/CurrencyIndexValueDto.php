<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CurrencyIndexValueDto extends AbstractItemDto
{
    public ?string $value;
    public ?string $currencyIndex;
    public ?string $bank;
}
