<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CurrencyIndex\Dao\CurrencyIndexValue;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CurrencyIndexValueListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CurrencyIndexValueDto::class)]
    public ?array $items = [];
}
