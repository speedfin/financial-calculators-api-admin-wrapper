<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanCorrectionMatrixRateAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_correction_matrix_rates'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_correction_matrix_rates')
        );
    }
}