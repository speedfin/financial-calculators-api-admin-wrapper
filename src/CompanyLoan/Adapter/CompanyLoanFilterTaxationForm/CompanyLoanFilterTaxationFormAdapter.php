<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterTaxationForm;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanFilterTaxationFormAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_taxation_forms'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_taxation_forms/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_taxation_forms/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_taxation_forms/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_taxation_forms')
        );
    }
}