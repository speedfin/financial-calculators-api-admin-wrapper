<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoan;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/company_loans/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loans'),
            sprintf('%s%s', $apiDomain, '/admin/company_loans/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loans/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/company_loans')
        );
    }
}