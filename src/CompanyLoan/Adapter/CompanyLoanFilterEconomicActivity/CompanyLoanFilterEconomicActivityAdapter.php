<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterEconomicActivity;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanFilterEconomicActivityAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_economic_activities'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_economic_activities/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_economic_activities/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_economic_activities/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_economic_activities')
        );
    }
}