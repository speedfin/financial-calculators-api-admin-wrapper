<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanFixedInterestRateMarginAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_fixed_interest_rate_margins'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_fixed_interest_rate_margins')
        );
    }
}