<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterSecurityType;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanFilterSecurityTypeAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_security_types'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_security_types/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_security_types/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_security_types/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_filter_security_types')
        );
    }
}