<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanMargin;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CompanyLoanMarginAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/company_loan_margins'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/company_loan_margins')
        );
    }
}