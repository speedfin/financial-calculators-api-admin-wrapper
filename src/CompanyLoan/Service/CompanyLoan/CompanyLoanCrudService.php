<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoan;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoan\CompanyLoanAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanListDto;

class CompanyLoanCrudService extends AbstractCrudService implements CompanyLoanCrudServiceInterface
{
    public function __construct(
        CompanyLoanAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanDto $itemDto): CompanyLoanDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new CompanyLoanDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanDto $itemDto): CompanyLoanDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new CompanyLoanDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CompanyLoanDto
    {
        return new CompanyLoanDto($this->getOneItem($id));
    }

    public function getAll(CompanyLoanFilter $filter): CompanyLoanListDto
    {
        return new CompanyLoanListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function convertDateTimeToStringBeforeSend(CompanyLoanDto $itemDto)
    {
        $itemDto->promotionDateFrom = $itemDto->promotionDateFrom instanceof \DateTime ?
            $itemDto->promotionDateFrom->format('Y-m-d') :
            $itemDto->promotionDateFrom;
        $itemDto->promotionDateTo = $itemDto->promotionDateTo instanceof \DateTime ?
            $itemDto->promotionDateTo->format('Y-m-d') :
            $itemDto->promotionDateTo ;
    }
}