<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoan;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\CompanyLoanListDto;

interface CompanyLoanCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(CompanyLoanDto $itemDto): CompanyLoanDto;
    public function create(CompanyLoanDto $itemDto): CompanyLoanDto;
    public function getOne(int $id): CompanyLoanDto;
    public function getAll(CompanyLoanFilter $filter): CompanyLoanListDto;
}