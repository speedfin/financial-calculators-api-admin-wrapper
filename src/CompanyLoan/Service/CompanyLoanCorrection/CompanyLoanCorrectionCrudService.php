<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanCorrection;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanCorrection\CompanyLoanCorrectionAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionFieldsDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionListDto;

class CompanyLoanCorrectionCrudService extends AbstractCrudService implements CompanyLoanCorrectionCrudServiceInterface
{
    public function __construct(
        CompanyLoanCorrectionAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanCorrectionDto $itemDto): CompanyLoanCorrectionDto
    {
        return new CompanyLoanCorrectionDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanCorrectionDto $itemDto): CompanyLoanCorrectionDto
    {
        return new CompanyLoanCorrectionDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CompanyLoanCorrectionDto
    {
        return new CompanyLoanCorrectionDto($this->getOneItem($id));
    }

    public function getAll(CompanyLoanCorrectionFilter $filter): CompanyLoanCorrectionListDto
    {
        return new CompanyLoanCorrectionListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): CompanyLoanCorrectionFieldsDto
    {
        return new CompanyLoanCorrectionFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}