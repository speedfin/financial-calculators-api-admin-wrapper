<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionFieldsDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection\CompanyLoanCorrectionListDto;

interface CompanyLoanCorrectionCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanCorrectionDto $itemDto): CompanyLoanCorrectionDto;
    public function create(CompanyLoanCorrectionDto $itemDto): CompanyLoanCorrectionDto;
    public function getOne(int $id): CompanyLoanCorrectionDto;
    public function getAll(CompanyLoanCorrectionFilter $filter): CompanyLoanCorrectionListDto;
    public function getFields(): CompanyLoanCorrectionFieldsDto;
}