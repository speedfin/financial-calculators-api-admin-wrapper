<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanMargin;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanMargin\CompanyLoanMarginAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginListDto;

class CompanyLoanMarginCrudService extends AbstractCrudService implements CompanyLoanMarginCrudServiceInterface
{
    public function __construct(
        CompanyLoanMarginAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanMarginDto $itemDto): CompanyLoanMarginDto
    {
        return new CompanyLoanMarginDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanMarginDto $itemDto): CompanyLoanMarginDto
    {
        return new CompanyLoanMarginDto($this->updateItem($itemDto));
    }

    public function getAll(CompanyLoanMarginFilter $filter): CompanyLoanMarginListDto
    {
        return new CompanyLoanMarginListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}