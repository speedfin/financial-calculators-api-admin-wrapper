<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanMargin;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin\CompanyLoanMarginListDto;

interface CompanyLoanMarginCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanMarginDto $itemDto): CompanyLoanMarginDto;
    public function create(CompanyLoanMarginDto $itemDto): CompanyLoanMarginDto;
    public function getAll(CompanyLoanMarginFilter $filter): CompanyLoanMarginListDto;
}