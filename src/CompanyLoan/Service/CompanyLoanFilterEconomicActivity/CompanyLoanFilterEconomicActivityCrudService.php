<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterEconomicActivity;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityListDto;

class CompanyLoanFilterEconomicActivityCrudService extends AbstractCrudService implements CompanyLoanFilterEconomicActivityCrudServiceInterface
{
    public function __construct(
        CompanyLoanFilterEconomicActivityAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanFilterEconomicActivityDto $itemDto): CompanyLoanFilterEconomicActivityDto
    {
        return new CompanyLoanFilterEconomicActivityDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanFilterEconomicActivityDto $itemDto): CompanyLoanFilterEconomicActivityDto
    {
        return new CompanyLoanFilterEconomicActivityDto($this->updateItem($itemDto));
    }

    public function getAll(CompanyLoanFilterEconomicActivityFilter $filter): CompanyLoanFilterEconomicActivityListDto
    {
        return new CompanyLoanFilterEconomicActivityListDto([
            'items' => $this->getAllItems($filter),
        ]);
    }
}