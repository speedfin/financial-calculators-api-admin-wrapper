<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterEconomicActivity;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity\CompanyLoanFilterEconomicActivityListDto;

interface CompanyLoanFilterEconomicActivityCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanFilterEconomicActivityDto $itemDto): CompanyLoanFilterEconomicActivityDto;
    public function create(CompanyLoanFilterEconomicActivityDto $itemDto): CompanyLoanFilterEconomicActivityDto;
    public function getAll(CompanyLoanFilterEconomicActivityFilter $filter): CompanyLoanFilterEconomicActivityListDto;
}