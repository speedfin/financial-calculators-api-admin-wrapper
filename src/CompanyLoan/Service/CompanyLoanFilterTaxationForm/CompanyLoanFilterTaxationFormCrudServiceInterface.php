<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterTaxationForm;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormListDto;

interface CompanyLoanFilterTaxationFormCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanFilterTaxationFormDto $itemDto): CompanyLoanFilterTaxationFormDto;
    public function create(CompanyLoanFilterTaxationFormDto $itemDto): CompanyLoanFilterTaxationFormDto;
    public function getAll(CompanyLoanFilterTaxationFormFilter $filter): CompanyLoanFilterTaxationFormListDto;
}