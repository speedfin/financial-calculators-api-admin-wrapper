<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterTaxationForm;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm\CompanyLoanFilterTaxationFormListDto;

class CompanyLoanFilterTaxationFormCrudService extends AbstractCrudService implements CompanyLoanFilterTaxationFormCrudServiceInterface
{
    public function __construct(
        CompanyLoanFilterTaxationFormAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanFilterTaxationFormDto $itemDto): CompanyLoanFilterTaxationFormDto
    {
        return new CompanyLoanFilterTaxationFormDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanFilterTaxationFormDto $itemDto): CompanyLoanFilterTaxationFormDto
    {
        return new CompanyLoanFilterTaxationFormDto($this->updateItem($itemDto));
    }

    public function getAll(CompanyLoanFilterTaxationFormFilter $filter): CompanyLoanFilterTaxationFormListDto
    {
        return new CompanyLoanFilterTaxationFormListDto([
            'items' => $this->getAllItems($filter),
        ]);
    }
}