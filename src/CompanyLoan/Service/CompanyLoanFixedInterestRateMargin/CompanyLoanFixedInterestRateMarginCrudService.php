<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginListDto;

class CompanyLoanFixedInterestRateMarginCrudService extends AbstractCrudService implements CompanyLoanFixedInterestRateMarginCrudServiceInterface
{
    public function __construct(CompanyLoanFixedInterestRateMarginAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanFixedInterestRateMarginDto $itemDto): CompanyLoanFixedInterestRateMarginDto
    {
        return new CompanyLoanFixedInterestRateMarginDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanFixedInterestRateMarginDto $itemDto): CompanyLoanFixedInterestRateMarginDto
    {
        return new CompanyLoanFixedInterestRateMarginDto($this->updateItem($itemDto));
    }

    public function getAll(CompanyLoanFixedInterestRateMarginFilter $filter): CompanyLoanFixedInterestRateMarginListDto
    {
        return new CompanyLoanFixedInterestRateMarginListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}
