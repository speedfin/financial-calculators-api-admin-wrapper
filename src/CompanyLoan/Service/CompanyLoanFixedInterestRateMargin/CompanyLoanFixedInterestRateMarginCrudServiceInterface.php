<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin\CompanyLoanFixedInterestRateMarginListDto;

interface CompanyLoanFixedInterestRateMarginCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanFixedInterestRateMarginDto $itemDto): CompanyLoanFixedInterestRateMarginDto;
    public function create(CompanyLoanFixedInterestRateMarginDto $itemDto): CompanyLoanFixedInterestRateMarginDto;
    public function getAll(CompanyLoanFixedInterestRateMarginFilter $filter): CompanyLoanFixedInterestRateMarginListDto;
}