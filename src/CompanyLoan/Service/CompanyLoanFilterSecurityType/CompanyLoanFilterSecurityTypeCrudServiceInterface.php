<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterSecurityType;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeListDto;

interface CompanyLoanFilterSecurityTypeCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanFilterSecurityTypeDto $itemDto): CompanyLoanFilterSecurityTypeDto;
    public function create(CompanyLoanFilterSecurityTypeDto $itemDto): CompanyLoanFilterSecurityTypeDto;
    public function getAll(CompanyLoanFilterSecurityTypeFilter $filter): CompanyLoanFilterSecurityTypeListDto;
}