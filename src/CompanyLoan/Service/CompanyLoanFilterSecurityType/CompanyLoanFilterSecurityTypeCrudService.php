<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanFilterSecurityType;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType\CompanyLoanFilterSecurityTypeListDto;

class CompanyLoanFilterSecurityTypeCrudService extends AbstractCrudService implements CompanyLoanFilterSecurityTypeCrudServiceInterface
{
    public function __construct(
        CompanyLoanFilterSecurityTypeAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanFilterSecurityTypeDto $itemDto): CompanyLoanFilterSecurityTypeDto
    {
        return new CompanyLoanFilterSecurityTypeDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanFilterSecurityTypeDto $itemDto): CompanyLoanFilterSecurityTypeDto
    {
        return new CompanyLoanFilterSecurityTypeDto($this->updateItem($itemDto));
    }

    public function getAll(CompanyLoanFilterSecurityTypeFilter $filter): CompanyLoanFilterSecurityTypeListDto
    {
        return new CompanyLoanFilterSecurityTypeListDto([
            'items' => $this->getAllItems($filter),
        ]);
    }
}