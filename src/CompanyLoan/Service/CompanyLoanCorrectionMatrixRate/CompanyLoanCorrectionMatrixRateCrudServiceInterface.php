<?php

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateListDto;

interface CompanyLoanCorrectionMatrixRateCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(CompanyLoanCorrectionMatrixRateDto $itemDto): CompanyLoanCorrectionMatrixRateDto;
    public function create(CompanyLoanCorrectionMatrixRateDto $itemDto): CompanyLoanCorrectionMatrixRateDto;
    public function getOne(int $id): CompanyLoanCorrectionMatrixRateDto;
    public function getAll(CompanyLoanCorrectionMatrixRateFilter $filter): CompanyLoanCorrectionMatrixRateListDto;
}