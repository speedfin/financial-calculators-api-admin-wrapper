<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Service\CompanyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\CompanyLoan\Adapter\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateAdapter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateListDto;

class CompanyLoanCorrectionMatrixRateCrudService extends AbstractCrudService implements CompanyLoanCorrectionMatrixRateCrudServiceInterface
{
    public function __construct(
        CompanyLoanCorrectionMatrixRateAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(CompanyLoanCorrectionMatrixRateDto $itemDto): CompanyLoanCorrectionMatrixRateDto
    {
        return new CompanyLoanCorrectionMatrixRateDto($this->createItem($itemDto));
    }

    public function update(CompanyLoanCorrectionMatrixRateDto $itemDto): CompanyLoanCorrectionMatrixRateDto
    {
        return new CompanyLoanCorrectionMatrixRateDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CompanyLoanCorrectionMatrixRateDto
    {
        return new CompanyLoanCorrectionMatrixRateDto($this->getOneItem($id));
    }

    public function getAll(CompanyLoanCorrectionMatrixRateFilter $filter): CompanyLoanCorrectionMatrixRateListDto
    {
        return new CompanyLoanCorrectionMatrixRateListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}