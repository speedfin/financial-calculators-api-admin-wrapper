<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionMatrixRateDto;

class CompanyLoanCorrectionMatrixRateDto extends AbstractProductCorrectionMatrixRateDto
{
    public ?string $companyLoanCorrection;

    public function setCompanyLoanCorrectionIRI(?string $companyLoanCorrectionIRI): self
    {
        $this->companyLoanCorrection = $companyLoanCorrectionIRI;

        return $this;
    }
}