<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CompanyLoanCorrectionMatrixRateFilter extends AbstractFilter
{
    public ?int $companyLoanCorrection;
    
    public function setCompanyLoanCorrectionId(?int $companyLoanCorrectionId): self
    {
        $this->companyLoanCorrection = $companyLoanCorrectionId;

        return $this;
    }
}