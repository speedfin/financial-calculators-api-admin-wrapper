<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanCorrectionMatrixRateListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanCorrectionMatrixRateDto::class)]
    public ?array $items = [];
}