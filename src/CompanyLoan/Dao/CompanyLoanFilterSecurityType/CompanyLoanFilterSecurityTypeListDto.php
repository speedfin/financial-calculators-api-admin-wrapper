<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanFilterSecurityTypeListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanFilterSecurityTypeDto::class)]
    public ?array $items = [];
}