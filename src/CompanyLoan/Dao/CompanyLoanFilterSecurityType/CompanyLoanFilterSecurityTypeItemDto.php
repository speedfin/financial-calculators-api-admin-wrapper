<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Admin\Common\Dao\Caster\BooleanCaster;

class CompanyLoanFilterSecurityTypeItemDto extends DataTransferObject
{
    #[CastWith(BooleanCaster::class)]
    public ?bool $enabled;
    public ?float $ltvFrom;
    public ?float $ltvTo;
}