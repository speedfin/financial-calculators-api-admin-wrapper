<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterSecurityType;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CompanyLoanFilterSecurityTypeDto extends AbstractItemDto
{
    public ?string $companyLoan;
    public ?CompanyLoanFilterSecurityTypeItemDto $undevelopedPlot;
    public ?CompanyLoanFilterSecurityTypeItemDto $residentialProperty;
    public ?CompanyLoanFilterSecurityTypeItemDto $productionProperty;
    public ?CompanyLoanFilterSecurityTypeItemDto $serviceProperty;
    public ?CompanyLoanFilterSecurityTypeItemDto $agriculturalParcel;
    public ?CompanyLoanFilterSecurityTypeItemDto $other;
    public ?CompanyLoanFilterSecurityTypeItemDto $deposit;

    public function setCompanyLoanIRI(?string $companyLoanIRI): CompanyLoanFilterSecurityTypeDto
    {
        $this->companyLoan = $companyLoanIRI;

        return $this;
    }
}