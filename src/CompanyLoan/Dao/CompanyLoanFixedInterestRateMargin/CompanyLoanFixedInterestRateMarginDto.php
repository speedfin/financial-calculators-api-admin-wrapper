<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\NullCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductMarginDto;

class CompanyLoanFixedInterestRateMarginDto extends AbstractProductMarginDto
{
    #[CastWith(NullCaster::class)]
    public ?string $ltvFrom;
    #[CastWith(NullCaster::class)]
    public ?string $ltvTo;
    #[CastWith(IntCaster::class)]
    public ?int $incomeFrom;
    #[CastWith(IntCaster::class)]
    public ?int $incomeTo;
    #[CastWith(IntCaster::class)]
    public ?int $creditPeriodFrom;
    #[CastWith(IntCaster::class)]
    public ?int $creditPeriodTo;
    #[CastWith(NullCaster::class)]
    public ?string $ltcFrom;
    #[CastWith(NullCaster::class)]
    public ?string $ltcTo;
    #[CastWith(NullCaster::class)]
    public ?string $costPerStandby;
    #[CastWith(NullCaster::class)]
    public ?string $costForRenewal;
    #[CastWith(IntCaster::class)]
    public ?int $periodForRenewal;
    #[CastWith(IntCaster::class)]
    public ?int $earlyRepaymentPeriod;
    #[CastWith(NullCaster::class)]
    public ?string $earlyRepaymentCost;
    #[CastWith(NullCaster::class)]
    public ?string $commissionForConsideration;
    #[CastWith(NullCaster::class)]
    public ?string $commissionForCommitment;
    #[CastWith(IntCaster::class)]
    public ?int $periodTo;
    #[CastWith(NullCaster::class)]
    public ?string $fixedInterestRate;
    #[CastWith(NullCaster::class)]
    public ?string $companyLoan;

    public function setCompanyLoanIRI(?string $companyLoanIRI): void
    {
        $this->companyLoan = $companyLoanIRI;
    }
}
