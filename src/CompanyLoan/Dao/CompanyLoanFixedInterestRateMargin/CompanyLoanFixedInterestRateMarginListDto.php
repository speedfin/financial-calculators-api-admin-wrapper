<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFixedInterestRateMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanFixedInterestRateMarginListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanFixedInterestRateMarginDto::class)]
    public ?array $items = [];
}