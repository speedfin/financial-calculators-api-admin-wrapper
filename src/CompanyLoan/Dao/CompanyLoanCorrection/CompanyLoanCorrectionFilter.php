<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CompanyLoanCorrectionFilter extends AbstractFilter
{
    public ?int $companyLoan;
    public ?int $firm;

    public function setCompanyLoanId(?int $companyLoanId): self
    {
        $this->companyLoan = $companyLoanId;

        return $this;
    }
}