<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanCorrectionListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanCorrectionDto::class)]
    public ?array $items = [];
}