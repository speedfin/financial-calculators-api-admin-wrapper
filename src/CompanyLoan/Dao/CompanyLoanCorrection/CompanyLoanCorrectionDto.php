<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrectionMatrixRate\CompanyLoanCorrectionMatrixRateDto;

class CompanyLoanCorrectionDto extends AbstractProductCorrectionDto
{
    public ?string $companyLoan;
    public ?string $paymentType;
    public ?string $paymentBase;
    public ?string $paymentBaseBuffer;
    public ?bool $forceCreditingPossibility;
    public ?bool $forceCrediting;
    public ?bool $addMarginToFixedInterestRateInterest;
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanCorrectionMatrixRateDto::class)]
    public ?array $matrixRates;
    
    public function setCompanyLoanIRI(?string $companyLoanId): self
    {
        $this->companyLoan = $companyLoanId;

        return $this;
    }
}