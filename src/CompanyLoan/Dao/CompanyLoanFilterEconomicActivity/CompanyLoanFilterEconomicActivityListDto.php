<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanFilterEconomicActivityListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanFilterEconomicActivityDto::class)]
    public ?array $items = [];
}