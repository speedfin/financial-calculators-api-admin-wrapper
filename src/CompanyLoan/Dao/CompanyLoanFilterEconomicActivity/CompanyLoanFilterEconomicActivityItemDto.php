<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Admin\Common\Dao\Caster\BooleanCaster;

class CompanyLoanFilterEconomicActivityItemDto extends DataTransferObject
{
    #[CastWith(BooleanCaster::class)]
    public ?bool $enabled;
    public ?int $companyStartUpFrom;
}