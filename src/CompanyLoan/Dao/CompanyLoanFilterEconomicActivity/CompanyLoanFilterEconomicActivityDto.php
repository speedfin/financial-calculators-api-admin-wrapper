<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterEconomicActivity;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CompanyLoanFilterEconomicActivityDto extends AbstractItemDto
{
    public ?string $companyLoan;
    public ?CompanyLoanFilterEconomicActivityItemDto $jdg;
    public ?CompanyLoanFilterEconomicActivityItemDto $generalPartnership;
    public ?CompanyLoanFilterEconomicActivityItemDto $civil;
    public ?CompanyLoanFilterEconomicActivityItemDto $limitedPartnership;
    public ?CompanyLoanFilterEconomicActivityItemDto $partnership;
    public ?CompanyLoanFilterEconomicActivityItemDto $zoo;
    public ?CompanyLoanFilterEconomicActivityItemDto $jointStockCompany;
    public ?CompanyLoanFilterEconomicActivityItemDto $agriculturalActivity;
    public ?CompanyLoanFilterEconomicActivityItemDto $housingAssociation;

    public function setCompanyLoanIRI(?string $companyLoanIRI): CompanyLoanFilterEconomicActivityDto
    {
        $this->companyLoan = $companyLoanIRI;

        return $this;
    }
}