<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanMarginListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanMarginDto::class)]
    public ?array $items = [];
}