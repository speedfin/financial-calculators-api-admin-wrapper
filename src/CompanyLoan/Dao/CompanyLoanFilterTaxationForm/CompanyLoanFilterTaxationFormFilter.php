<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CompanyLoanFilterTaxationFormFilter extends AbstractFilter
{
    public ?int $companyLoan;

    public function setCompanyLoanId(?int $companyLoanId): self
    {
        $this->companyLoan = $companyLoanId;

        return $this;
    }
}