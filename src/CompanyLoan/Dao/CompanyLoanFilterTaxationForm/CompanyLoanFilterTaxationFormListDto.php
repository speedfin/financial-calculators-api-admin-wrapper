<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanFilterTaxationFormListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanFilterTaxationFormDto::class)]
    public ?array $items = [];
}