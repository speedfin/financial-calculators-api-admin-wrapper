<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CompanyLoanFilterTaxationFormDto extends AbstractItemDto
{
    public ?string $companyLoan;
    public ?CompanyLoanFilterTaxationFormItemDto $kpir;
    public ?CompanyLoanFilterTaxationFormItemDto $lumpSum;
    public ?CompanyLoanFilterTaxationFormItemDto $taxCard;
    public ?CompanyLoanFilterTaxationFormItemDto $fullAccountancy;
    public ?CompanyLoanFilterTaxationFormItemDto $repairFund;
    public ?CompanyLoanFilterTaxationFormItemDto $agriculturalTax;

    public function setCompanyLoanIRI(?string $companyLoanIRI): CompanyLoanFilterTaxationFormDto
    {
        $this->companyLoan = $companyLoanIRI;

        return $this;
    }
}