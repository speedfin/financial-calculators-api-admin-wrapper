<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanFilterTaxationForm;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Admin\Common\Dao\Caster\BooleanCaster;

class CompanyLoanFilterTaxationFormItemDto extends DataTransferObject
{
    #[CastWith(BooleanCaster::class)]
    public ?bool $enabled;
    public ?int $incomeFrom;
    public ?int $incomeTo;
}