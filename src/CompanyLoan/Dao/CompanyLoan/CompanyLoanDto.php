<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto\DescriptionFieldsDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto\InfoFieldsDto;
use Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto\VariousFilterDto;

class CompanyLoanDto extends AbstractProductDto
{
    const INTEREST_TYPES = [
        self::VARIABLE_INTEREST_RATE_TYPE => 'zmienne',
        self::FIXED_INTEREST_RATE_TYPE => 'stałe'
    ];

    const VARIABLE_INTEREST_RATE_TYPE = 'variable_interest_rate';
    const FIXED_INTEREST_RATE_TYPE = 'fixed_interest_rate';

    public string $interestType = self::VARIABLE_INTEREST_RATE_TYPE;
    public string $relatedType = 'companyLoan';
    public ?string $productType;
    public ?bool $forceProvisionCredited;
    public bool $checkProvisionCredited = false;
    public bool $secured = false;
    public bool $equalInstallmentEnabled = true;
    public bool $decreasingInstallmentEnabled = true;
    public bool $ltcEnabled = true;
    public ?int $constructionPeriod;
    public bool $disableCreditPeriod = false;
    public bool $specialOffer = false;
    public ?InfoFieldsDto $infoFields;
    public ?DescriptionFieldsDto $descriptionFields;
    public ?VariousFilterDto $variousFilter;
}