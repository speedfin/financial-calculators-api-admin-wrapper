<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto;

use Spatie\DataTransferObject\DataTransferObject;

class DescriptionFieldsDto extends DataTransferObject
{
    public ?string $requiredMinimumAccountRecipients;
    public ?string $additionalIncome;
    public ?string $companyProductsConsolidation;
    public ?string $consolidationOfConsumerProducts;
    public ?string $methodOfEstimatingCapacity;
    public ?string $capacityCalculation;
    public ?string $consentOfSpouse;
}