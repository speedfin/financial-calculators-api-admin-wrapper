<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto;

use Spatie\DataTransferObject\DataTransferObject;

class InfoFieldsDto extends DataTransferObject
{
    public ?string $margin;
    public ?string $provision;
    public ?string $costPerStandby;
    public ?string $costForRenewal;
    public ?string $earlyRepaymentCost;
    public ?string $commissionForConsideration;
    public ?string $commissionForCommitment;
    public ?string $gracePeriod;
    public ?string $secureValue;
    public ?string $ltv;
    public ?string $creditPeriod;
}