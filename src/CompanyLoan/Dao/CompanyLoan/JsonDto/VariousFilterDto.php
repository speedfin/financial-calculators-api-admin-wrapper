<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan\JsonDto;

use Spatie\DataTransferObject\DataTransferObject;

class VariousFilterDto extends DataTransferObject
{
    public ?bool $companyProductsConsolidation;
    public ?bool $consumerProductsConsolidation;
    public ?bool $additionalIncome;
    public ?bool $spouseConsent;
}