<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CompanyLoanListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CompanyLoanDto::class)]
    public ?array $items = [];
}