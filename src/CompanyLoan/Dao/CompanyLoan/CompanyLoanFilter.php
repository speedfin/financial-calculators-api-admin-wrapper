<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoan;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CompanyLoanFilter extends AbstractFilter
{
    public bool $archived = false;

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
}