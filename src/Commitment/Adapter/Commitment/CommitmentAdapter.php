<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Adapter\Commitment;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class CommitmentAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/commitments/{id}';
    public string $apiDomain;
    
    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/commitments'),
            sprintf('%s%s', $apiDomain, '/admin/commitments/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/commitments/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/commitments')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/commitments/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}
