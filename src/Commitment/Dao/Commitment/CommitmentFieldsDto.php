<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Dao\Commitment;

use Spatie\DataTransferObject\DataTransferObject;

class CommitmentFieldsDto extends DataTransferObject
{
    public array $fields;
}
