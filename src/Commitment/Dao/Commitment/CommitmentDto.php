<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Dao\Commitment;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class CommitmentDto extends AbstractItemDto
{
    public ?string $name;
    public ?string $description;
    public ?bool $visible;
    public ?array $fields;
}
