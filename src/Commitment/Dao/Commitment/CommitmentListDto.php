<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Dao\Commitment;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CommitmentListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CommitmentDto::class)]
    public ?array $items = [];
}
