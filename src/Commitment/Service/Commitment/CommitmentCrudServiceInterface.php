<?php

namespace Speedfin\Calculators\Admin\Commitment\Service\Commitment;

use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentDto;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentFieldsDto;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentFilter;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface CommitmentCrudServiceInterface extends JWTInterface
{
    public function update(CommitmentDto $itemDto): CommitmentDto;
    public function create(CommitmentDto $itemDto): CommitmentDto;
    public function getOne(int $id): CommitmentDto;
    public function getAll(CommitmentFilter $filter): CommitmentListDto;
    public function getFields(): CommitmentFieldsDto;
}
