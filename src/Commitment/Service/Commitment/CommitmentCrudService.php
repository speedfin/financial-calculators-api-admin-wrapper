<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Service\Commitment;

use Speedfin\Calculators\Admin\Commitment\Adapter\Commitment\CommitmentAdapter;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentDto;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentFieldsDto;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Commitment\Dao\Commitment\CommitmentListDto;

class CommitmentCrudService extends AbstractCrudService implements CommitmentCrudServiceInterface
{
    public function __construct(CommitmentAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(CommitmentDto $itemDto): CommitmentDto
    {
        return new CommitmentDto($this->createItem($itemDto));
    }

    public function update(CommitmentDto $itemDto): CommitmentDto
    {
        return new CommitmentDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CommitmentDto
    {
        return new CommitmentDto($this->getOneItem($id));
    }
    
    public function getAll(CommitmentFilter $filter): CommitmentListDto
    {
        return new CommitmentListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): CommitmentFieldsDto
    {
        return new CommitmentFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
