<?php

namespace Speedfin\Calculators\Admin\Currency\Service\Currency;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyDto;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyFilter;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyListDto;

interface CurrencyCrudServiceInterface extends JWTInterface
{
    public function update(CurrencyDto $itemDto): CurrencyDto;
    public function create(CurrencyDto $itemDto): CurrencyDto;
    public function getOne(int $id): CurrencyDto;
    public function getAll(CurrencyFilter $filter): CurrencyListDto;
}
