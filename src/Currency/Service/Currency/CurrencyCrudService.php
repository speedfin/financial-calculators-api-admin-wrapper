<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Currency\Service\Currency;

use Speedfin\Calculators\Admin\Currency\Adapter\Currency\CurrencyAdapter;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyDto;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Currency\Dao\Currency\CurrencyListDto;

class CurrencyCrudService extends AbstractCrudService implements CurrencyCrudServiceInterface
{
    public function __construct(CurrencyAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(CurrencyDto $itemDto): CurrencyDto
    {
        return new CurrencyDto($this->createItem($itemDto));
    }

    public function update(CurrencyDto $itemDto): CurrencyDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new CurrencyDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): CurrencyDto
    {
        return new CurrencyDto($this->getOneItem($id));
    }
    
    public function getAll(CurrencyFilter $filter): CurrencyListDto
    {
        return new CurrencyListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function convertDateTimeToStringBeforeSend(CurrencyDto $itemDto)
    {
        $itemDto->apiUpdated = $itemDto->apiUpdated instanceof \DateTime ?
            $itemDto->apiUpdated->format('Y-m-d H:i:s') :
            $itemDto->apiUpdated;
    }
}
