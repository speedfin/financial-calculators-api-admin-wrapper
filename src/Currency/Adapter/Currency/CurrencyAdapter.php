<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Currency\Adapter\Currency;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class CurrencyAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/currencies/{id}';
    
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/currencies'),
            sprintf('%s%s', $apiDomain, '/admin/currencies/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/currencies/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/currencies')
        );
    }
}
