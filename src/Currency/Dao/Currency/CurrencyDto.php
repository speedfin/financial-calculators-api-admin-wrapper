<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Currency\Dao\Currency;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;
use Speedfin\Calculators\Admin\Common\Dao\Caster\DateTimeCaster;

class CurrencyDto extends AbstractItemDto
{
    public ?string $name;
    public ?string $shortName;
    public ?string $symbol;
    public ?bool $visible;
    public ?bool $defaultSelected;
    public ?int $useOrder;
    public ?array $currencyIndexes;
    public ?float $currentValue;
    #[CastWith(DateTimeCaster::class, format: 'Y-m-d H:i:s')]
    public $apiUpdated;
}
