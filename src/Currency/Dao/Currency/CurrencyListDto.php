<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Currency\Dao\Currency;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class CurrencyListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: CurrencyDto::class)]
    public ?array $items = [];
}
