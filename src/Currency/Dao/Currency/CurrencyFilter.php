<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Currency\Dao\Currency;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CurrencyFilter extends AbstractFilter
{
    public ?int $bankId;
    public ?bool $visible;

    public function setBankId(?int $bankId): self
    {
        $this->bankId = $bankId;

        return $this;
    }
}
