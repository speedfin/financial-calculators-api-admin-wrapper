<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Adapter\BankEmployment;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class BankEmploymentAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/bank_employments/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/bank_employments'),
            sprintf('%s%s', $apiDomain, '/admin/bank_employments/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/bank_employments/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/bank_employments')
        );
    }
}
