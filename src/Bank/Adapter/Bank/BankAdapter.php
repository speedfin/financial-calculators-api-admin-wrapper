<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Adapter\Bank;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class BankAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/banks/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/banks'),
            sprintf('%s%s', $apiDomain, '/admin/banks/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/banks/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/banks')
        );
    }
}
