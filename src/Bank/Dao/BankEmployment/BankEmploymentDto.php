<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\BankEmployment;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class BankEmploymentDto extends AbstractItemDto
{
    public ?int $employmentId;
    public ?string $description;
    public ?bool $displayDescription;
    public ?bool $visible;
    public ?array $data;
    public ?string $bank;
    public ?string $employment;
}
