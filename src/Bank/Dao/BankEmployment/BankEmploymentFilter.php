<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\BankEmployment;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class BankEmploymentFilter extends AbstractFilter
{
    public ?int $bank;
}
