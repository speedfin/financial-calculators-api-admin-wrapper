<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\BankEmployment;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class BankEmploymentListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: BankEmploymentDto::class)]
    public ?array $items = [];
}
