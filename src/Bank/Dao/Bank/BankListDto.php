<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\Bank;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class BankListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: BankDto::class)]
    public ?array $items = [];
}
