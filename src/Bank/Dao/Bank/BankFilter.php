<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\Bank;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class BankFilter extends AbstractFilter
{
    public ?bool $visible;
    public mixed $id;
    public ?string $firmsMortgage;
    public ?string $firmsMortgageLoan;
    public ?string $firmsMoneyLoan;
    public ?string $firmsCompanyLoan;
}
