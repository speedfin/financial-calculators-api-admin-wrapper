<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Dao\Bank;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class BankDto extends AbstractItemDto
{
    public ?string $name;
    public ?string $logo;
    public ?string $slug;
    public ?string $description;
    public ?string $shortDescription;
    public ?array $currencies;
    public ?array $currencyIndexValues;
    public ?int $creditingPeriodMin;
    public ?int $creditingPeriodMax;
    public ?int $clientAgeMin;
    public ?int $clientAgeMax;
    public ?int $ltvMax;
    public ?int $processingTime;
    public ?bool $visible;
    public ?bool $mortgageAllowed;
    public ?bool $mortgageLoanAllowed;
    public ?bool $moneyLoanAllowed;
}
