<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Service\BankEmployment;

use Speedfin\Calculators\Admin\Bank\Adapter\BankEmployment\BankEmploymentAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentDto;
use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentListDto;

class BankEmploymentCrudService extends AbstractCrudService implements BankEmploymentCrudServiceInterface
{
    public function __construct(BankEmploymentAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(BankEmploymentDto $itemDto): BankEmploymentDto
    {
        return new BankEmploymentDto($this->createItem($itemDto));
    }

    public function update(BankEmploymentDto $itemDto): BankEmploymentDto
    {
        return new BankEmploymentDto($this->updateItem($itemDto));
    }
    
    public function getOne(int $id): BankEmploymentDto
    {
        return new BankEmploymentDto($this->getOneItem($id));
    }

    public function getAll(BankEmploymentFilter $filter): BankEmploymentListDto
    {
        return new BankEmploymentListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}