<?php

namespace Speedfin\Calculators\Admin\Bank\Service\BankEmployment;

use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentDto;
use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentFilter;
use Speedfin\Calculators\Admin\Bank\Dao\BankEmployment\BankEmploymentListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface BankEmploymentCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(BankEmploymentDto $itemDto): BankEmploymentDto;
    public function create(BankEmploymentDto $itemDto): BankEmploymentDto;
    public function getOne(int $id): BankEmploymentDto;
    public function getAll(BankEmploymentFilter $filter): BankEmploymentListDto;
}
