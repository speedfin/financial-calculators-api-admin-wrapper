<?php

namespace Speedfin\Calculators\Admin\Bank\Service\Bank;

use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankFilter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface BankCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(BankDto $itemDto): BankDto;
    public function create(BankDto $itemDto): BankDto;
    public function getOne(int $id): ?BankDto;
    public function getAll(BankFilter $filter): BankListDto;
}
