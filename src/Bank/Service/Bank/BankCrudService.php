<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Bank\Service\Bank;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankListDto;

class BankCrudService extends AbstractCrudService implements BankCrudServiceInterface
{
    public function __construct(BankAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(BankDto $itemDto): BankDto
    {
        return new BankDto($this->createItem($itemDto));
    }

    public function update(BankDto $itemDto): BankDto
    {
        return new BankDto($this->updateItem($itemDto));
    }
    
    public function getOne(int $id): ?BankDto
    {
        $array = $this->getOneItem($id);
        return $array ? new BankDto($array) : null;
    }

    public function getAll(BankFilter $filter): BankListDto
    {
        return new BankListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}