<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Firm\Adapter\Firm;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class FirmAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/firms/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/firms'),
            sprintf('%s%s', $apiDomain, '/admin/firms/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/firms/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/firms')
        );
    }

    public function getOneByUuidRequest(string $uuid, string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            str_replace('{id}', (string) $uuid, $this->getOneAPIEndpoint),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }

    public function updateByUuidRequest(string $uuid, array $data, string $token): CurlResponse
    {
        return $this->client->request(
            'PATCH',
            str_replace('{id}', (string) $uuid, $this->updateAPIEndpoint),
            [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-type' => 'application/merge-patch+json'
                ]
            ]
        );
    }
}
