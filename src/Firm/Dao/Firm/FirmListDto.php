<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Firm\Dao\Firm;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class FirmListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: FirmDto::class)]
    public ?array $items = [];
}
