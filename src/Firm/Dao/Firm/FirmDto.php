<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Firm\Dao\Firm;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class FirmDto extends AbstractItemDto
{
    public ?string $uuid = null;
    public ?array $banksMortgage;
    public ?array $banksMortgageLoan;
    public ?array $banksMoneyLoan;
    public ?array $banksCompanyLoan;
}
