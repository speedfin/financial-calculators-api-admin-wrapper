<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Firm\Service\Firm;

use Speedfin\Calculators\Admin\Firm\Adapter\Firm\FirmAdapter;
use Speedfin\Calculators\Admin\Firm\Dao\Firm\FirmDto;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;

class FirmCrudService extends AbstractCrudService implements FirmCrudServiceInterface
{
    public function __construct(FirmAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(FirmDto $itemDto): FirmDto
    {
        return new FirmDto($this->createItem($itemDto));
    }

    public function update(FirmDto $itemDto): FirmDto
    {
        return new FirmDto($this->adapter->updateByUuidRequest($itemDto->uuid, $itemDto->toArray(), $this->token)->toArray());
    }
    
    public function getOne(string $uuid): FirmDto
    {
        return new FirmDto($this->adapter->getOneByUuidRequest($uuid, $this->token)->toArray());
    }
}
