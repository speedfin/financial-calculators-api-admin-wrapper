<?php

namespace Speedfin\Calculators\Admin\Firm\Service\Firm;

use Speedfin\Calculators\Admin\Firm\Dao\Firm\FirmDto;
use Speedfin\Calculators\Admin\Firm\Dao\Firm\FirmFilter;
use Speedfin\Calculators\Admin\Firm\Dao\Firm\FirmListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface FirmCrudServiceInterface extends JWTInterface
{
    public function update(FirmDto $itemDto): FirmDto;
    public function create(FirmDto $itemDto): FirmDto;
    public function getOne(string $uuid): FirmDto;
}
