<?php

namespace Speedfin\Calculators\Admin\Product\Service\Product;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Product\Dao\Product\ProductFilter;
use Speedfin\Calculators\Admin\Product\Dao\Product\ProductListDto;

interface ProductCrudServiceInterface extends JWTInterface
{
    public function getAll(ProductFilter $filter): ProductListDto;
}
