<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Product\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class ProductListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: ProductDto::class)]
    public ?array $items = [];
}
