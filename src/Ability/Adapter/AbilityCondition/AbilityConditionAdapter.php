<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Adapter\AbilityCondition;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class AbilityConditionAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/ability_conditions/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/ability_conditions'),
            sprintf('%s%s', $apiDomain, '/admin/ability_conditions/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/ability_conditions/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/ability_conditions')
        );
    }
}
