<?php

namespace Speedfin\Calculators\Admin\Ability\Service\AbilityModule;

use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFieldsDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFilter;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface AbilityModuleCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(AbilityModuleDto $itemDto): AbilityModuleDto;
    public function create(AbilityModuleDto $itemDto): AbilityModuleDto;
    public function getOne(int $id): AbilityModuleDto;
    public function getAll(AbilityModuleFilter $filter): AbilityModuleListDto;
    public function getFields(): AbilityModuleFieldsDto;
}
