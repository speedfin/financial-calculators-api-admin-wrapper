<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Service\AbilityModule;

use Speedfin\Calculators\Admin\Ability\Adapter\AbilityModule\AbilityModuleAdapter;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFieldsDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleListDto;

class AbilityModuleCrudService extends AbstractCrudService implements AbilityModuleCrudServiceInterface
{
    public function __construct(AbilityModuleAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(AbilityModuleDto $itemDto): AbilityModuleDto
    {
        return new AbilityModuleDto($this->createItem($itemDto));
    }

    public function update(AbilityModuleDto $itemDto): AbilityModuleDto
    {
        return new AbilityModuleDto($this->updateItem($itemDto));
    }
    
    public function getOne(int $id): AbilityModuleDto
    {
        return new AbilityModuleDto($this->getOneItem($id));
    }

    public function getAll(AbilityModuleFilter $filter): AbilityModuleListDto
    {
        return new AbilityModuleListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): AbilityModuleFieldsDto
    {
        return new AbilityModuleFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
