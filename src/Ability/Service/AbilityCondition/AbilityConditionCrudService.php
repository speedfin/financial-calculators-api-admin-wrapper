<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Service\AbilityCondition;

use Speedfin\Calculators\Admin\Ability\Adapter\AbilityCondition\AbilityConditionAdapter;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionListDto;

class AbilityConditionCrudService extends AbstractCrudService implements AbilityConditionCrudServiceInterface
{
    public function __construct(AbilityConditionAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(AbilityConditionDto $itemDto): AbilityConditionDto
    {
        return new AbilityConditionDto($this->createItem($itemDto));
    }

    public function update(AbilityConditionDto $itemDto): AbilityConditionDto
    {
        return new AbilityConditionDto($this->updateItem($itemDto));
    }
    
    public function getOne(int $id): AbilityConditionDto
    {
        return new AbilityConditionDto($this->getOneItem($id));
    }

    public function getAll(AbilityConditionFilter $filter): AbilityConditionListDto
    {
        return new AbilityConditionListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}
