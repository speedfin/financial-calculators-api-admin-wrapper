<?php

namespace Speedfin\Calculators\Admin\Ability\Service\AbilityCondition;

use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionDto;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionFilter;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition\AbilityConditionListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface AbilityConditionCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(AbilityConditionDto $itemDto): AbilityConditionDto;
    public function create(AbilityConditionDto $itemDto): AbilityConditionDto;
    public function getOne(int $id): AbilityConditionDto;
    public function getAll(AbilityConditionFilter $filter): AbilityConditionListDto;
}
