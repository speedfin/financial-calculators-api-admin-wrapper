<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Service\Ability;

use Speedfin\Calculators\Admin\Ability\Adapter\Ability\AbilityAdapter;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityDto;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityFieldsDto;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityListDto;

class AbilityCrudService extends AbstractCrudService implements AbilityCrudServiceInterface
{
    public function __construct(
        AbilityAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(AbilityDto $itemDto): AbilityDto
    {
        return new AbilityDto($this->createItem($itemDto));
    }

    public function update(AbilityDto $itemDto): AbilityDto
    {
        return new AbilityDto($this->updateItem($itemDto));
    }
    
    public function getOne(int $id): AbilityDto
    {
        return new AbilityDto($this->getOneItem($id));
    }

    public function getAll(AbilityFilter $filter): AbilityListDto
    {
        return new AbilityListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): AbilityFieldsDto
    {
        return new AbilityFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
