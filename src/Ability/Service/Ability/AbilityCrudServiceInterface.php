<?php

namespace Speedfin\Calculators\Admin\Ability\Service\Ability;

use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityDto;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityFieldsDto;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityFilter;
use Speedfin\Calculators\Admin\Ability\Dao\Ability\AbilityListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface AbilityCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(AbilityDto $itemDto): AbilityDto;
    public function create(AbilityDto $itemDto): AbilityDto;
    public function getOne(int $id): AbilityDto;
    public function getAll(AbilityFilter $filter): AbilityListDto;
    public function getFields(): AbilityFieldsDto;
}
