<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class AbilityConditionFilter extends AbstractFilter
{
    public ?int $ability;
}
