<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class AbilityConditionListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: AbilityConditionDto::class)]
    public ?array $items = [];
}
