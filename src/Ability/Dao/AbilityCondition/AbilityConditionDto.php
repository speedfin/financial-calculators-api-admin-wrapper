<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityCondition;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class AbilityConditionDto extends AbstractItemDto
{
    public ?string $ability;
    public ?int $abilityId;
    public ?string $formula;
    public ?string $variable;
    public ?bool $active = true;
}
