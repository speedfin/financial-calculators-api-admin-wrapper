<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\Ability;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class AbilityFilter extends AbstractFilter
{
    public ?int $bank;
    public ?string $parent;
}
