<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\Ability;

use Spatie\DataTransferObject\DataTransferObject;

class AbilityFieldsDto extends DataTransferObject
{
    public array $currencyIndexes;
    public array $margins;

}