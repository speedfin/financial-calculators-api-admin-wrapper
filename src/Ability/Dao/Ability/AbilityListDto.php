<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\Ability;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class AbilityListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: AbilityDto::class)]
    public ?array $items = [];
}