<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\Ability;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class AbilityDto extends AbstractItemDto
{
    public ?string $marginType;
    public ?string $marginValue;
    public ?string $currencyIndexType;
    public ?string $abilityRatio;
    public ?string $incomeRatio;
    public ?string $info;
    public ?string $bufferArray;
    public ?int $period;
    public ?bool $active;
    public ?string $parent;
    public ?string $bank;
    public ?int $bankId;

}
