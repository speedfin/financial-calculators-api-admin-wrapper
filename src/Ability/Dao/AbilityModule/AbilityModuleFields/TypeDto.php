<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFields;

use Spatie\DataTransferObject\DataTransferObject;

class TypeDto extends DataTransferObject
{
    public string $type;
    public string $label;
    public string $variable;
}