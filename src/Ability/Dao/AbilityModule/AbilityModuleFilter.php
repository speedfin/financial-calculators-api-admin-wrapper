<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityModule;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class AbilityModuleFilter extends AbstractFilter
{
    public ?int $ability;
    public ?string $type;
}
