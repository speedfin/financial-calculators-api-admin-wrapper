<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityModule;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class AbilityModuleDto extends AbstractItemDto
{
    public ?string $ability;
    public ?int $abilityId;
    public ?string $type;
    public ?string $variable;
    public ?string $data;
    public ?bool $active = false;
}
