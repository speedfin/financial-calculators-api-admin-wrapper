<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeDto;

class MortgageCreditPurposeListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageCreditPurposeDto::class)]
    public ?array $items = [];
}