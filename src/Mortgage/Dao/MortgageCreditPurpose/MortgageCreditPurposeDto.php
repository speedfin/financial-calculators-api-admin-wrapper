<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class MortgageCreditPurposeDto extends AbstractItemDto
{
    public ?string $name;
    public ?int $useOrder;
    public ?bool $visible;
}