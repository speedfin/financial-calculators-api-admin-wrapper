<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageCreditPurposeFilter extends AbstractFilter
{
    public ?bool $visible;

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }
}