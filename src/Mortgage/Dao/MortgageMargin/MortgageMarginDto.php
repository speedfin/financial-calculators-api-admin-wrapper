<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Speedfin\Calculators\Admin\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\NullCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductMarginDto;

class MortgageMarginDto extends AbstractProductMarginDto
{
    #[CastWith(NullCaster::class)]
    public ?string $ltvFrom;
    #[CastWith(NullCaster::class)]
    public ?string $ltvTo;
    #[CastWith(IntCaster::class)]
    public ?int $incomeFrom;
    #[CastWith(IntCaster::class)]
    public ?int $incomeTo;
    #[CastWith(NullCaster::class)]
    public ?string $mortgage;
    

    public function setMortgageIRI(?string $mortgageIRI): void
    {
        $this->mortgage = $mortgageIRI;
    }
}
