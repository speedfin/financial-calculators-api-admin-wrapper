<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageMarginFilter extends AbstractFilter
{
    public ?int $mortgage;

    public function setMortgageId(?int $mortgageId): self
    {
        $this->mortgage = $mortgageId;

        return $this;
    }
}