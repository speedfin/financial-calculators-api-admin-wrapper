<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginDto;

class MortgageMarginListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageMarginDto::class)]
    public ?array $items = [];
}