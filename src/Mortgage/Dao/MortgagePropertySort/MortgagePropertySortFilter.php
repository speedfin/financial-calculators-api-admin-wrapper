<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgagePropertySortFilter extends AbstractFilter
{
    public ?bool $visible;
    public ?array $types;

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function addType(?int $type): self
    {
        $this->types[] = $type;

        return $this;
    }
}