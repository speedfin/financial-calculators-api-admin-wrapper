<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortDto;

class MortgagePropertySortListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgagePropertySortDto::class)]
    public ?array $items = [];
}