<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class MortgagePropertySortDto extends AbstractItemDto
{
    public ?string $name;
    public ?int $useOrder;
    public ?array $types;
    public ?int $primaryMarket;
    public ?bool $visible;
}