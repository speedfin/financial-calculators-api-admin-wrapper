<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateDto;

class MortgageCorrectionMatrixRateListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageCorrectionMatrixRateDto::class)]
    public ?array $items = [];
}