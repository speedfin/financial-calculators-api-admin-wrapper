<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionDto;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionMatrixRateDto;

class MortgageCorrectionMatrixRateDto extends AbstractProductCorrectionMatrixRateDto
{
    public ?string $mortgageCorrection;

    public function setMortgageCorrectionIRI(?string $mortgageCorrectionIRI): self
    {
        $this->mortgageCorrection = $mortgageCorrectionIRI;

        return $this;
    }
}