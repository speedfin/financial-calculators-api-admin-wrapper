<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageCorrectionMatrixRateFilter extends AbstractFilter
{
    public ?int $mortgageCorrection;
    
    public function setMortgageCorrectionId(?int $mortgageCorrectionId): self
    {
        $this->mortgageCorrection = $mortgageCorrectionId;

        return $this;
    }
}