<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MortgageFixedInterestRateMarginListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageFixedInterestRateMarginDto::class)]
    public ?array $items = [];
}