<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageFixedInterestRateMarginFilter extends AbstractFilter
{
    public ?int $mortgage;

    public function setMortgageId(?int $mortgageId): self
    {
        $this->mortgage = $mortgageId;

        return $this;
    }
}