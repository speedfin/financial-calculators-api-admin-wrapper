<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionDto;

class MortgageCorrectionListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageCorrectionDto::class)]
    public ?array $items = [];
}