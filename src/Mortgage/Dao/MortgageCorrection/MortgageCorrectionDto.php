<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateDto;

class MortgageCorrectionDto extends AbstractProductCorrectionDto
{
    public ?string $mortgage;
    public ?string $paymentType = "";
    public ?string $paymentBase = "";
    public ?bool $forceCreditingPossibility;
    public ?bool $forceCrediting;
    public ?bool $addMarginToFixedInterestRateInterest;
    #[CastWith(ArrayCaster::class, itemType: MortgageCorrectionMatrixRateDto::class)]
    public ?array $matrixRates;
    
    public function setMortgageIRI(?string $mortgageId): self
    {
        $this->mortgage = $mortgageId;

        return $this;
    }
}