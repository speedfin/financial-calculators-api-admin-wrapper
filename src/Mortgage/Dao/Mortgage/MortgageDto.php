<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductDto;

class MortgageDto extends AbstractProductDto
{
    const INTEREST_TYPES = [
        self::VARIABLE_INTEREST_RATE_TYPE => 'zmienne',
        self::FIXED_INTEREST_RATE_TYPE => 'stałe'
    ];

    const VARIABLE_INTEREST_RATE_TYPE = 'variable_interest_rate';
    const FIXED_INTEREST_RATE_TYPE = 'fixed_interest_rate';

    public ?bool $loan;
    public string $interestType = self::VARIABLE_INTEREST_RATE_TYPE;
    public string $relatedType = 'Mortgage';
    public ?bool $forceProvisionCredited;
    public bool $checkProvisionCredited = false;
    public ?int $constructionPeriod;
    public ?array $mortgageCreditPurpose = [];
    public ?array $mortgagePropertySorts = [];
    public ?bool $eco = false;

    public function isFixedInterestRate()
    {
        return $this->interestType === self::FIXED_INTEREST_RATE_TYPE;
    }
    
    public function markAsLoan()
    {
        $this->loan = true;
    }
}