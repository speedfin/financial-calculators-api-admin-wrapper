<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageDto;

class MortgageListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageDto::class)]
    public ?array $items = [];
}