<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionCombinedDto;

class MortgageCorrectionCombinedDto extends AbstractProductCorrectionCombinedDto
{
    public ?string $mortgage;
    public ?bool $addMarginToFixedInterestRate;

    public function setMortgageIRI(?string $mortgageId): self
    {
        $this->mortgage = $mortgageId;

        return $this;
    }
}