<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedDto;

class MortgageCorrectionCombinedListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MortgageCorrectionCombinedDto::class)]
    public ?array $items = [];
}