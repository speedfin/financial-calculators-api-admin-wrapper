<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgagePropertySort;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgagePropertySortAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/mortgage_property_sorts/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_property_sorts'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_property_sorts/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_property_sorts/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_property_sorts')
        );
    }
}