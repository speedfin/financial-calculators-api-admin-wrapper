<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgageCorrectionMatrixRateAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_matrix_rates'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_matrix_rates')
        );
    }
}