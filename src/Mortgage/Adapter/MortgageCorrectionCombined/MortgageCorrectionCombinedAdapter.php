<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrectionCombined;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class MortgageCorrectionCombinedAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/mortgage_correction_combineds/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_combineds'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_combineds/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_combineds/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_correction_combineds')
        );
    }
}