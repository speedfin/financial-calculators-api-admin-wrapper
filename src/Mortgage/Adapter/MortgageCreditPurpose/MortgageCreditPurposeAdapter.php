<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCreditPurpose;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgageCreditPurposeAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/mortgage_credit_purposes/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_credit_purposes'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_credit_purposes/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_credit_purposes/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_credit_purposes')
        );
    }
}