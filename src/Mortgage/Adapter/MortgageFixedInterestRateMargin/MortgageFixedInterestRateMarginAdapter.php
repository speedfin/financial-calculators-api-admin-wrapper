<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgageFixedInterestRateMarginAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_fixed_interest_rate_margins'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_fixed_interest_rate_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_fixed_interest_rate_margins')
        );
    }
}