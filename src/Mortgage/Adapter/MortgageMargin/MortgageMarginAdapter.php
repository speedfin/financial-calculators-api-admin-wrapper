<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageMargin;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgageMarginAdapter extends AbstractAdapter
{
    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_margins'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_margins/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_margins')
        );
    }
}