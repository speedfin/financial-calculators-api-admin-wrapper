<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\Mortgage;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageListDto;

interface MortgageCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(MortgageDto $itemDto): MortgageDto;
    public function create(MortgageDto $itemDto): MortgageDto;
    public function getOne(int $id): MortgageDto;
    public function getAll(MortgageFilter $filter): MortgageListDto;
}