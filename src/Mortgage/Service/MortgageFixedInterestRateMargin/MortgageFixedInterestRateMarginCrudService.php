<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginListDto;

class MortgageFixedInterestRateMarginCrudService extends AbstractCrudService implements MortgageFixedInterestRateMarginCrudServiceInterface
{
    public function __construct(
        MortgageFixedInterestRateMarginAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageFixedInterestRateMarginDto $itemDto): MortgageFixedInterestRateMarginDto
    {
        return new MortgageFixedInterestRateMarginDto($this->createItem($itemDto));
    }

    public function update(MortgageFixedInterestRateMarginDto $itemDto): MortgageFixedInterestRateMarginDto
    {
        return new MortgageFixedInterestRateMarginDto($this->updateItem($itemDto));
    }

    public function getAll(MortgageFixedInterestRateMarginFilter $filter): MortgageFixedInterestRateMarginListDto
    {
        return new MortgageFixedInterestRateMarginListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}