<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageFixedInterestRateMargin;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageFixedInterestRateMargin\MortgageFixedInterestRateMarginListDto;

interface MortgageFixedInterestRateMarginCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MortgageFixedInterestRateMarginDto $itemDto): MortgageFixedInterestRateMarginDto;
    public function create(MortgageFixedInterestRateMarginDto $itemDto): MortgageFixedInterestRateMarginDto;
    public function getAll(MortgageFixedInterestRateMarginFilter $filter): MortgageFixedInterestRateMarginListDto;
}