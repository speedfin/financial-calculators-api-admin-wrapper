<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgagePropertySort;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortListDto;

interface MortgagePropertySortCrudServiceInterface extends JWTInterface
{
    //public function delete(int $id): void;
    public function update(MortgagePropertySortDto $itemDto): MortgagePropertySortDto;
    public function create(MortgagePropertySortDto $itemDto): MortgagePropertySortDto;
    public function getOne(int $id): MortgagePropertySortDto;
    public function getAll(MortgagePropertySortFilter $filter): MortgagePropertySortListDto;
}