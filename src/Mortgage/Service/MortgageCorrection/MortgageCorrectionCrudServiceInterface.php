<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrection;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionFieldsDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionListDto;

interface MortgageCorrectionCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MortgageCorrectionDto $itemDto): MortgageCorrectionDto;
    public function create(MortgageCorrectionDto $itemDto): MortgageCorrectionDto;
    public function getOne(int $id): MortgageCorrectionDto;
    public function getAll(MortgageCorrectionFilter $filter): MortgageCorrectionListDto;
    public function getFields(): MortgageCorrectionFieldsDto;
}