<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateListDto;

class MortgageCorrectionMatrixRateCrudService extends AbstractCrudService implements MortgageCorrectionMatrixRateCrudServiceInterface
{
    public function __construct(
        MortgageCorrectionMatrixRateAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageCorrectionMatrixRateDto $itemDto): MortgageCorrectionMatrixRateDto
    {
        return new MortgageCorrectionMatrixRateDto($this->createItem($itemDto));
    }

    public function update(MortgageCorrectionMatrixRateDto $itemDto): MortgageCorrectionMatrixRateDto
    {
        return new MortgageCorrectionMatrixRateDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgageCorrectionMatrixRateDto
    {
        return new MortgageCorrectionMatrixRateDto($this->getOneItem($id));
    }

    public function getAll(MortgageCorrectionMatrixRateFilter $filter): MortgageCorrectionMatrixRateListDto
    {
        return new MortgageCorrectionMatrixRateListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}