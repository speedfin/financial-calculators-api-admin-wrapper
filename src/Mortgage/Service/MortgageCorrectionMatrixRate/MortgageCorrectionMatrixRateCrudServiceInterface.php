<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionMatrixRate\MortgageCorrectionMatrixRateListDto;

interface MortgageCorrectionMatrixRateCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MortgageCorrectionMatrixRateDto $itemDto): MortgageCorrectionMatrixRateDto;
    public function create(MortgageCorrectionMatrixRateDto $itemDto): MortgageCorrectionMatrixRateDto;
    public function getOne(int $id): MortgageCorrectionMatrixRateDto;
    public function getAll(MortgageCorrectionMatrixRateFilter $filter): MortgageCorrectionMatrixRateListDto;
}