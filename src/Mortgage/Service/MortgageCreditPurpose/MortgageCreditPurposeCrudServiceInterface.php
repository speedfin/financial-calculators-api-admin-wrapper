<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCreditPurpose;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeListDto;

interface MortgageCreditPurposeCrudServiceInterface extends JWTInterface
{
    //public function delete(int $id): void;
    public function update(MortgageCreditPurposeDto $itemDto): MortgageCreditPurposeDto;
    public function create(MortgageCreditPurposeDto $itemDto): MortgageCreditPurposeDto;
    public function getOne(int $id): MortgageCreditPurposeDto;
    public function getAll(MortgageCreditPurposeFilter $filter): MortgageCreditPurposeListDto;
}