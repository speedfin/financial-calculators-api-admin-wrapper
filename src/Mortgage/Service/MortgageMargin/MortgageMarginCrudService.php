<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageMargin;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageMargin\MortgageMarginAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginListDto;

class MortgageMarginCrudService extends AbstractCrudService implements MortgageMarginCrudServiceInterface
{
    public function __construct(
        MortgageMarginAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageMarginDto $itemDto): MortgageMarginDto
    {
        return new MortgageMarginDto($this->createItem($itemDto));
    }

    public function update(MortgageMarginDto $itemDto): MortgageMarginDto
    {
        return new MortgageMarginDto($this->updateItem($itemDto));
    }

    public function getAll(MortgageMarginFilter $filter): MortgageMarginListDto
    {
        return new MortgageMarginListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}