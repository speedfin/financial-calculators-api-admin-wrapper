<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageMargin;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageMargin\MortgageMarginListDto;

interface MortgageMarginCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MortgageMarginDto $itemDto): MortgageMarginDto;
    public function create(MortgageMarginDto $itemDto): MortgageMarginDto;
    public function getAll(MortgageMarginFilter $filter): MortgageMarginListDto;
}