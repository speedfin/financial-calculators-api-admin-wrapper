<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrectionCombined;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrectionCombined\MortgageCorrectionCombinedAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedListDto;

class MortgageCorrectionCombinedCrudService extends AbstractCrudService implements MortgageCorrectionCombinedCrudServiceInterface
{
    public function __construct(
        MortgageCorrectionCombinedAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageCorrectionCombinedDto $itemDto): MortgageCorrectionCombinedDto
    {
        return new MortgageCorrectionCombinedDto($this->createItem($itemDto));
    }

    public function update(MortgageCorrectionCombinedDto $itemDto): MortgageCorrectionCombinedDto
    {
        return new MortgageCorrectionCombinedDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgageCorrectionCombinedDto
    {
        return new MortgageCorrectionCombinedDto($this->getOneItem($id));
    }

    public function getAll(MortgageCorrectionCombinedFilter $filter): MortgageCorrectionCombinedListDto
    {
        return new MortgageCorrectionCombinedListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}