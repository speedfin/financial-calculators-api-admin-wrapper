<?php

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrectionCombined;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedFieldsDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrectionCombined\MortgageCorrectionCombinedListDto;

interface MortgageCorrectionCombinedCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MortgageCorrectionCombinedDto $itemDto): MortgageCorrectionCombinedDto;
    public function create(MortgageCorrectionCombinedDto $itemDto): MortgageCorrectionCombinedDto;
    public function getOne(int $id): MortgageCorrectionCombinedDto;
    public function getAll(MortgageCorrectionCombinedFilter $filter): MortgageCorrectionCombinedListDto;
}