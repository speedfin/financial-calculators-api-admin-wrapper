<?php

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanFilter;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterListDto;

interface MoneyLoanFilterCrudServiceInterface extends JWTInterface
{
//    public function delete(int $id): void;
    public function update(MoneyLoanFilterDto $itemDto): MoneyLoanFilterDto;
    public function create(MoneyLoanFilterDto $itemDto): MoneyLoanFilterDto;
    public function getOne(int $id): MoneyLoanFilterDto;
    public function getAll(MoneyLoanFilterFilter $filter): MoneyLoanFilterListDto;
    public function getFields(): MoneyLoanFilterFieldsDto;
}
