<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanFilter;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanFilter\MoneyLoanFilterAdapter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter\MoneyLoanFilterListDto;

class MoneyLoanFilterCrudService extends AbstractCrudService implements MoneyLoanFilterCrudServiceInterface
{
    public function __construct(MoneyLoanFilterAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(MoneyLoanFilterDto $itemDto): MoneyLoanFilterDto
    {
        return new MoneyLoanFilterDto($this->createItem($itemDto));
    }

    public function update(MoneyLoanFilterDto $itemDto): MoneyLoanFilterDto
    {
        return new MoneyLoanFilterDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MoneyLoanFilterDto
    {
        return new MoneyLoanFilterDto($this->getOneItem($id));
    }

    public function getAll(MoneyLoanFilterFilter $filter): MoneyLoanFilterListDto
    {
        return new MoneyLoanFilterListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): MoneyLoanFilterFieldsDto
    {
        return new MoneyLoanFilterFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
