<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateAdapter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateListDto;

class MoneyLoanCorrectionMatrixRateCrudService extends AbstractCrudService implements MoneyLoanCorrectionMatrixRateCrudServiceInterface
{
    public function __construct(
        MoneyLoanCorrectionMatrixRateAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MoneyLoanCorrectionMatrixRateDto $itemDto): MoneyLoanCorrectionMatrixRateDto
    {
        return new MoneyLoanCorrectionMatrixRateDto($this->createItem($itemDto));
    }

    public function update(MoneyLoanCorrectionMatrixRateDto $itemDto): MoneyLoanCorrectionMatrixRateDto
    {
        return new MoneyLoanCorrectionMatrixRateDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MoneyLoanCorrectionMatrixRateDto
    {
        return new MoneyLoanCorrectionMatrixRateDto($this->getOneItem($id));
    }

    public function getAll(MoneyLoanCorrectionMatrixRateFilter $CorrectionMatrixRate): MoneyLoanCorrectionMatrixRateListDto
    {
        return new MoneyLoanCorrectionMatrixRateListDto([
            'items' => $this->getAllItems($CorrectionMatrixRate)
        ]);
    }
}
