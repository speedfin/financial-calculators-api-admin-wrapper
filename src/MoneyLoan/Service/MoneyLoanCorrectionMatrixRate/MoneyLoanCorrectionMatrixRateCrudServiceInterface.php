<?php

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate\MoneyLoanCorrectionMatrixRateListDto;

interface MoneyLoanCorrectionMatrixRateCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MoneyLoanCorrectionMatrixRateDto $itemDto): MoneyLoanCorrectionMatrixRateDto;
    public function create(MoneyLoanCorrectionMatrixRateDto $itemDto): MoneyLoanCorrectionMatrixRateDto;
    public function getOne(int $id): MoneyLoanCorrectionMatrixRateDto;
    public function getAll(MoneyLoanCorrectionMatrixRateFilter $CorrectionMatrixRate): MoneyLoanCorrectionMatrixRateListDto;
}
