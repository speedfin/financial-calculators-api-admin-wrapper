<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanCorrection\MoneyLoanCorrectionAdapter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionListDto;

class MoneyLoanCorrectionCrudService extends AbstractCrudService implements MoneyLoanCorrectionCrudServiceInterface
{
    public function __construct(MoneyLoanCorrectionAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(MoneyLoanCorrectionDto $itemDto): MoneyLoanCorrectionDto
    {
        return new MoneyLoanCorrectionDto($this->createItem($itemDto));
    }

    public function update(MoneyLoanCorrectionDto $itemDto): MoneyLoanCorrectionDto
    {
        return new MoneyLoanCorrectionDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MoneyLoanCorrectionDto
    {
        return new MoneyLoanCorrectionDto($this->getOneItem($id));
    }

    public function getAll(MoneyLoanCorrectionFilter $Correction): MoneyLoanCorrectionListDto
    {
        return new MoneyLoanCorrectionListDto([
            'items' => $this->getAllItems($Correction)
        ]);
    }

    public function getFields(): MoneyLoanCorrectionFieldsDto
    {
        return new MoneyLoanCorrectionFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
