<?php

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection\MoneyLoanCorrectionListDto;

interface MoneyLoanCorrectionCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MoneyLoanCorrectionDto $itemDto): MoneyLoanCorrectionDto;
    public function create(MoneyLoanCorrectionDto $itemDto): MoneyLoanCorrectionDto;
    public function getOne(int $id): MoneyLoanCorrectionDto;
    public function getAll(MoneyLoanCorrectionFilter $Correction): MoneyLoanCorrectionListDto;
    public function getFields(): MoneyLoanCorrectionFieldsDto;
}
