<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoan;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoan\MoneyLoanAdapter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanListDto;

class MoneyLoanCrudService extends AbstractCrudService implements MoneyLoanCrudServiceInterface
{
    public function __construct(MoneyLoanAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(MoneyLoanDto $itemDto): MoneyLoanDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new MoneyLoanDto($this->createItem($itemDto));
    }

    public function update(MoneyLoanDto $itemDto): MoneyLoanDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new MoneyLoanDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MoneyLoanDto
    {
        return new MoneyLoanDto($this->getOneItem($id));
    }

    public function getAll(MoneyLoanFilter $filter): MoneyLoanListDto
    {
        return new MoneyLoanListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function convertDateTimeToStringBeforeSend(MoneyLoanDto $itemDto)
    {
        $itemDto->promotionDateFrom = $itemDto->promotionDateFrom instanceof \DateTime ?
            $itemDto->promotionDateFrom->format('Y-m-d') :
            $itemDto->promotionDateFrom;
        $itemDto->promotionDateTo = $itemDto->promotionDateTo instanceof \DateTime ?
            $itemDto->promotionDateTo->format('Y-m-d') :
            $itemDto->promotionDateTo ;
    }
}
