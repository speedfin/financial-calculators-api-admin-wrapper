<?php

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoan;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\MoneyLoanListDto;

interface MoneyLoanCrudServiceInterface extends JWTInterface
{
    public function update(MoneyLoanDto $itemDto): MoneyLoanDto;
    public function create(MoneyLoanDto $itemDto): MoneyLoanDto;
    public function getOne(int $id): MoneyLoanDto;
    public function getAll(MoneyLoanFilter $filter): MoneyLoanListDto;
}
