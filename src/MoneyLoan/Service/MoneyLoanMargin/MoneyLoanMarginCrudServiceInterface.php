<?php

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanMargin;

use Speedfin\Calculators\Admin\Common\Service\JWTInterface;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginListDto;

interface MoneyLoanMarginCrudServiceInterface extends JWTInterface
{
    public function delete(int $id): void;
    public function update(MoneyLoanMarginDto $itemDto): MoneyLoanMarginDto;
    public function create(MoneyLoanMarginDto $itemDto): MoneyLoanMarginDto;
    public function getOne(int $id): MoneyLoanMarginDto;
    public function getAll(MoneyLoanMarginFilter $Margin): MoneyLoanMarginListDto;
    public function getFields(): MoneyLoanMarginFieldsDto;
}
