<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Service\MoneyLoanMargin;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanMargin\MoneyLoanMarginAdapter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginFieldsDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginFilter;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin\MoneyLoanMarginListDto;

class MoneyLoanMarginCrudService extends AbstractCrudService implements MoneyLoanMarginCrudServiceInterface
{
    public function __construct(MoneyLoanMarginAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(MoneyLoanMarginDto $itemDto): MoneyLoanMarginDto
    {
        return new MoneyLoanMarginDto($this->createItem($itemDto));
    }

    public function update(MoneyLoanMarginDto $itemDto): MoneyLoanMarginDto
    {
        return new MoneyLoanMarginDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MoneyLoanMarginDto
    {
        return new MoneyLoanMarginDto($this->getOneItem($id));
    }

    public function getAll(MoneyLoanMarginFilter $Margin): MoneyLoanMarginListDto
    {
        return new MoneyLoanMarginListDto([
            'items' => $this->getAllItems($Margin)
        ]);
    }

    public function getFields(): MoneyLoanMarginFieldsDto
    {
        return new MoneyLoanMarginFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
