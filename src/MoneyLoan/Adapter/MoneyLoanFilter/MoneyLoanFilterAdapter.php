<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanFilter;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class MoneyLoanFilterAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/money_loan_filters/{id}';
    public string $apiDomain;

    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/money_loan_filters'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_filters/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_filters/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_filters')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/money_loan_filters/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}
