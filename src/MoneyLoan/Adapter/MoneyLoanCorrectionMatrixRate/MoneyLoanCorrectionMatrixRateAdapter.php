<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class MoneyLoanCorrectionMatrixRateAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/money_loan_correction_matrix_rates/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/money_loan_correction_matrix_rates'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_correction_matrix_rates/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_correction_matrix_rates')
        );
    }
}
