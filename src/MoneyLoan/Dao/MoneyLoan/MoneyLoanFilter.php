<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MoneyLoanFilter extends AbstractFilter
{
    public array|string|null $bank;
    public bool $archived = false;
    public ?bool $visible;

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
}
