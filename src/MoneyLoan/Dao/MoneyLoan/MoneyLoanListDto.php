<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MoneyLoanListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MoneyLoanDto::class)]
    public ?array $items = [];
}
