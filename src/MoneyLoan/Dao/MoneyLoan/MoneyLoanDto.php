<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductDto;
use Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\JsonDto\InfoFieldsDto;

class MoneyLoanDto extends AbstractProductDto
{
    public ?int $moneyLoanFilterId;
    public ?string $grossCreditValueCalculate;
    public ?string $insuranceDescription;
    public ?InfoFieldsDto $infoFields;
}
