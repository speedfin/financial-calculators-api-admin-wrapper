<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoan\JsonDto;

use Spatie\DataTransferObject\DataTransferObject;

class InfoFieldsDto extends DataTransferObject
{
    public ?string $interest;
    public ?string $provision;
}
