<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MoneyLoanMarginListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MoneyLoanMarginDto::class)]
    public ?array $items = [];
}
