<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Admin\Common\Dao\Caster\NullCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductMarginDto;

class MoneyLoanMarginDto extends AbstractProductMarginDto
{
    public ?string $moneyLoan;
    public ?string $interest;
    #[CastWith(NullCaster::class)]
    public ?string $marginMax;
    #[CastWith(NullCaster::class)]
    public ?string $provisionMin;
    #[CastWith(NullCaster::class)]
    public ?string $provisionMax;
    #[CastWith(IntCaster::class)]
    public ?int $creditingPeriodFrom;
    #[CastWith(IntCaster::class)]
    public ?int $creditingPeriodTo;
}
