<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanMargin;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MoneyLoanMarginFilter extends AbstractFilter
{
    public ?int $moneyLoan;
}
