<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MoneyLoanFilterListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MoneyLoanFilterDto::class)]
    public ?array $items = [];
}
