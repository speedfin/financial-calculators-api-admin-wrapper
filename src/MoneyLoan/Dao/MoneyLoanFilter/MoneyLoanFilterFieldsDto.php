<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter;

use Spatie\DataTransferObject\DataTransferObject;

class MoneyLoanFilterFieldsDto extends DataTransferObject
{
    public array $fields;
}
