<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MoneyLoanFilterFilter extends AbstractFilter
{
    public ?int $moneyLoan;
}
