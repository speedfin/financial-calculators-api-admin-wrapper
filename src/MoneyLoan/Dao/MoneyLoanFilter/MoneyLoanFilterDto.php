<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class MoneyLoanFilterDto extends AbstractItemDto
{
    public ?string $moneyLoan;
    public ?bool $internalClient;
    public ?bool $externalClient;
    public ?bool $consolidationOffer;
    public ?bool $promotion;
    public ?bool $freelance;
    public ?bool $uniformedServices;
    public ?bool $retirementDisability;
    public ?bool $contractOfEmployment;
    public ?bool $commissionWorkContract;
    public ?bool $economicActivity;
    public ?bool $incomeFromAbroad;
    public ?array $internalClientData;
    public ?array $externalClientData;
    public ?array $consolidationOfferData;
    public ?array $promotionData;
    public ?array $freelanceData;
    public ?array $uniformedServicesData;
    public ?array $retirementDisabilityData;
    public ?array $contractOfEmploymentData;
    public ?array $commissionWorkContractData;
    public ?array $economicActivityData;
    public ?array $incomeFromAbroadData;
}
