<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MoneyLoanCorrectionListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MoneyLoanCorrectionDto::class)]
    public ?array $items = [];
}
