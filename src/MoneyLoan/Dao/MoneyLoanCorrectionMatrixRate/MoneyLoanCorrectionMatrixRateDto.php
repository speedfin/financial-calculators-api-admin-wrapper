<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionMatrixRateDto;

class MoneyLoanCorrectionMatrixRateDto extends AbstractProductCorrectionMatrixRateDto
{
    public ?string $moneyLoanCorrection;
}
