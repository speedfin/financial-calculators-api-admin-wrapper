<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class MoneyLoanCorrectionMatrixRateListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: MoneyLoanCorrectionMatrixRateDto::class)]
    public ?array $items = [];
}
