<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrectionMatrixRate;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MoneyLoanCorrectionMatrixRateFilter extends AbstractFilter
{
    public ?int $moneyLoanCorrection;
}
