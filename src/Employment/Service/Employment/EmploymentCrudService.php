<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Employment\Service\Employment;

use Speedfin\Calculators\Admin\Employment\Adapter\Employment\EmploymentAdapter;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentDto;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentFieldsDto;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentFilter;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentListDto;

class EmploymentCrudService extends AbstractCrudService implements EmploymentCrudServiceInterface
{
    public function __construct(EmploymentAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create(EmploymentDto $itemDto): EmploymentDto
    {
        return new EmploymentDto($this->createItem($itemDto));
    }

    public function update(EmploymentDto $itemDto): EmploymentDto
    {
        return new EmploymentDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): ?EmploymentDto
    {
        return new EmploymentDto($this->getOneItem($id));
    }
    
    public function getAll(EmploymentFilter $filter): EmploymentListDto
    {
        return new EmploymentListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
    
    public function getFields(): EmploymentFieldsDto
    {
        return new EmploymentFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}
