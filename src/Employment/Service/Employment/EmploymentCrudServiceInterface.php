<?php

namespace Speedfin\Calculators\Admin\Employment\Service\Employment;

use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentDto;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentFieldsDto;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentFilter;
use Speedfin\Calculators\Admin\Employment\Dao\Employment\EmploymentListDto;
use Speedfin\Calculators\Admin\Common\Service\JWTInterface;

interface EmploymentCrudServiceInterface extends JWTInterface
{
    public function update(EmploymentDto $itemDto): EmploymentDto;
    public function create(EmploymentDto $itemDto): EmploymentDto;
    public function getOne(int $id): ?EmploymentDto;
    public function getAll(EmploymentFilter $filter): EmploymentListDto;
    public function getFields(): EmploymentFieldsDto;
}
