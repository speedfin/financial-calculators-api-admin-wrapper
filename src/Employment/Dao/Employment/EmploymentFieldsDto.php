<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Employment\Dao\Employment;

use Spatie\DataTransferObject\DataTransferObject;

class EmploymentFieldsDto extends DataTransferObject
{
    public array $fields;
}
