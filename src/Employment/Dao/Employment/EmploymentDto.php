<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Employment\Dao\Employment;

use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class EmploymentDto extends AbstractItemDto
{
    public ?string $name;
    public ?string $description;
    public ?bool $visible;
    public ?array $fields;
}
